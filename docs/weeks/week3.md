#03.

**Design for the Real World**

![](./assets/week3/DESIGN1.jpg)

This class has been important to us in different dimensions and was quite fun at the same time. Crafting and DIY have been a part of my professional job for several years so I was familiar with the scavenging, assembly part and using some of the machines. However, the first and most important matter that we had to deal with is the team building component that emerged from this week, which will have a lasting effect. It helped us get to know each other and learn more about our functioning as a team, referring to both the small groups and us as a class altogether.

There were several hiccups and miscommunications along the way within our group and I believe that that is due to the fact that no one wanted to declare themselves as the leader of the group. We were all being careful not to overstep on each other’s competencies without even knowing each other’s abilities well.

Our assignments shifted several times during the week and by the end our product was not 100% finished. We currently are still working on it and plan to finish it by the 6th of November. Outside classes and conflicting schedules between our lectures and machine availability have been a factor to postpone the actual realization.
Below I will explain the work flow of the week.
Brainstorm and Concept Design

The first step the team took was to do a brainstorming to identify the needs and wants of the space, having in mind both functionality and aesthetics.

The brainstorming part involved discussions among the team members  and then clustering the ideas according to categories such as: “Green Space”, “Recycling”, ”Modular Foldable furniture”, “Idea Space”, ”Light and Air”, “Maker Space” and other areas we thought were important to have in the classroom. Ideas have ranged from coat hangers, air ventilation, to common food and reading spaces and maker areas.

As a group we have identified the 3 most important areas we thought we should focus on by having in mind the space at our disposal and they are: Classroom seating area, Maker Space area, Chill Area and then smaller items such as green space, coat hangers and others.

**Some pictures that we were inspired from for the presentation:**

![](./assets/week3/DESIGN2.jpg)

![](./assets/week3/DESIGN8.jpg)

![](./assets/week3/DESIGN14.jpg)

The next step after this was to make a list of the inventory inside the classroom to understand better what we actually have and what we need to add on to the list.

Afterwards we made a basic concept of the space distribution of the class on Sketchup and made different proposals on how to use the space of the class with the objects we feel we might need.

The list of products varies from standalone working areas, to modular chairs and tables, tables for the FabLab and many other items. Finally, I and Tom were chosen to do the presentation which was prepared by Maite, with some input from the rest of the team. After the presentations our team was chosen to work on the Maker Space area, which included a working table, 3d printing table, tool organizers, a lamp and other accessories. Below I will show some pictures of the steps we went through in the project.


**Some of the scavenged Items we found for our project:**

![](./assets/week3/DESIGN22.jpeg)

![](./assets/week3/DESIGN23.jpeg)

**Inspirations for the table:**

![](./assets/week3/table1.jpeg)

![](./assets/week3/table2.jpeg)

![](./assets/week3/table3.jpeg)

After the presentation we sketched the products depending on the pieces of materials we had and the functionality of them. My main contribution to this project was concept design and functionality of the main working table, the accessories and the manual cutting/assembly part. I used hand sketches together with my team mate Maite to come up with designs and concepts for the functionality of the table and then the rest of the team visualized them in 3D using different software and made the files ready for the machine cutting.  Although I have been working with CNC and laser machines for a long time, I still have to learn how to operate the machines/prepare the files myself and am looking forward to the rest of the year to be able to do so.

**Our work in progress**

![](./assets/week3/DESIGN16.jpeg)

Also, I am grateful to have had the opportunity to understand the functioning and abilities of my fellow team members, as this will make cooperation in the future a lot easier.

**Sketches for the concept design and functionality**

![](./assets/week3/DESIGN88.jpeg)

![](./assets/week3/DESIGN21.jpeg)

![](./assets/week3/DESIGN7.jpeg)

![](./assets/week3/DESIGN10.jpeg)
