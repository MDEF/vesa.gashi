#Week7.

**The Way Things Work**


Vesa Gashi Week 7

The week of The Way Things Work was quite fun and inspiring.

The lectures of course were a bit too technical sometimes but nonetheless interesting and informative. The first two days we explored the world behind most of the products we use every day, by disassembling them and learning the programing behind them. Our team had a mission to break apart a printer and identify the objects. There were a lot of hard P7 plastic pieces that were completely useless in their function apart from serving as a shield/shape for the product, tiny motors, ink cartridges and other small parts.

**What I’ve learned is that the #7 plastic is one of the most toxic plastics out there and very harmful to humans.**

Also, an important thing about ink cartridges, they are supposed to be the main source of business for printer producers, having the printers cost cheap but then the ink cartridges expensive. Personally, I have been using hacked ink cartridges all my life, mainly due to the fact that there were no official retailers in my country so we had to rely on our autonomous skills. So the joke is on HP and the rest.

 ![](./assets/week7/PIC1.jpeg)
Disassembling process.

![](./assets/week7/PIC2.jpeg)
Wrong day to wear a white shirt.

 ![](./assets/week7/PIC3.jpeg)
(Almost) final breakdown of the printer.


Apart from these we have had a wonderful insight on matters such as Direct Current (DC), Alternating Current (AC), Analog vs Digital, Electronical vs Electromechanical, Binary vs Decimal and other concepts.

For illustration:
**Direct Current is used for lower voltage equipment such as phone, laptop etc.**
**Alternating Current is used higher voltage equipment such as AConditioner, etc.**
(You need to use a converter that converts energy from AC to DC because it will burn your phone)

Other interesting facts from the day:  50s concept of computers are the ones we use now
Founders of Arduino helped make accessible and understandable the electronic parts to novices and experts alike.

Processing is a software built at MIT- helped people program in an easier way and had revolutionized how things are built/designed.

**“Processing is a flexible software sketchbook and a language for learning how to code within the context of the visual arts. Since 2001, Processing has promoted software literacy within the visual arts and visual literacy within technology. There are tens of thousands of students, artists, designers, researchers, and hobbyists who use Processing for learning and prototyping.”** - https://processing.org/

Group Project
This was the most important part of the entire week. Separated in groups we came up with different projects how to combine Inputs and Outputs using Arduino and other components as needed for the projects.

Our group came up with the “Blue Wave” project; we were creating an output without knowing the input. At first it seemed like a challenge but we quickly overcame it.

The project included several moving pieces that had to be coordinated together, below I will explain in further detail:
Moving parts- We had to design and program 3 moving parts that would move the LED light strip consecutively in a long line, just like a wave, hence the name. We did this by combining small motor parts with Arduino, some straight wooden sticks and bits of glue and whatnot.
The LED light strip, programing it to change colors and speed. This was a hassle and we programmed more than 30 alternatives but ended up using only a few of them. Nonetheless it was really fun and we got seriously involved in it, loved it!
  In between the two parts there were a lot of trial and error steps, such as trying to solder LED strips (for hours), fixing the intensity of the electricity going through the board and finding ways to adjust/stabilize it, cables, cables, cables and more cables. All in all the highs and lows from thinking we got a break and then realizing we had it all wrong were nerve-wracking but somehow still fun.

 ![](./assets/week7/PIC8.jpeg)
The point when we learned that after three hours of working in a flooded Fab Lab, the soldering method we were using was COMPLETELY amateurish. Thanks to the guys at Fab Lab we learned to use the tools and are now self-proclaimed soldering masters.

 ![](./assets/week7/PIC6.jpeg)
A failed attempt to use microphones in our programing. This small baby was overly complicated, we switched to another piece of hardware/microphone after and it worked.

 ![](./assets/week7/PIC9.jpeg)
Testing and programing the led strip.

Our group had a lot of componentents: Building the moving pieces, programing them, mounting them on the ceiling, soldering LED strips, programing the led strips, fixing the voltage, preparing them for inputs and putting all of them to work together. Basically we were the last ones in class to have it all work, we were tired on a Friday night at class but it finally worked even though the end result was crazy looking we were happy with our little achievement as you can tell by the grin on our faces! 

![](./assets/week7/PIC10.jpeg)
