#09.

**Vesa Gashi**

Week 9

**Engaging Narratives**

![](./assets/week9/0.jpg)

This week has been one of the weeks I could connect with most because of my previous professional background, since we discussed about ways of promoting our projects through various channels. Sometimes when you come from a similar field yourself and you have been practicing those methodologies into your work, it’s actually a bit difficult to apply them to yourself instead of a client. I have worked in numerous projects and concepts into selling other peoples stories or products but I have always outsourced services if I ever needed those services for myself. To me it’s exciting and challenging to have to promote my own projects and ideas and make them appealing to others. Below I will share some of the highlights from the week.

**Heather Corcoran – Kickstarter**
London-based outreach lead for Europe, looks for projects that emphasize inventive design and fine craftsmanship.

Heather gave some useful tips and guidelines on how to write appealing narratives for crowd funding platforms, especially Kickstarter. The main tips I thought were interesting were to be simple, as into writing short descriptive sentences and preferably use appealing videos that would also be short and to the point. The other important tip was to try to make that short narrative personal and give users a reason to support you, make them feel as they are part of your journey or part of the problem solving. Incentives, are really important as well, so rewarding people who support you is one of the crucial elements. Incentives can be in form of recognitions, gifts, free product usage or else, depending on the project/product you are trying to sell/build.

I would’ve been super cool to have had Heather in class and be able to have informal chats about her experiences but hoping there would be other opportunities to do that.


**Bjarke Kalvin – Duckling**
Social entrepreneur. Founder and CEO of Duckling.

I loved Bjarkes approach and the way they at Duckling are trying to improvde the way we use social media platforms and turn them into productive customizable tools so they can serve us for a better purpose. Duckling is a startup that has the best intentions in doing so and I do hope it will be successful; it is a sort of a hybrid between Wikipedia and Instagram, plus at the same time allows you to use the tool and export the contents into various formats, which to me is very useful into making presentations and making them appealing without having to rely on the skills of a professional.

My narration below is the story we were asked to write and present during that class, I loved experimenting with the tools and researching what others have been doing.

![](./assets/week9/1.jpeg)
![](./assets/week9/2.jpeg)
![](./assets/week9/3.jpeg)
![](./assets/week9/3A.jpeg)
![](./assets/week9/4.jpeg)
![](./assets/week9/5.jpeg)
![](./assets/week9/6.jpeg)
![](./assets/week9/7.jpeg)
![](./assets/week9/8.jpeg)
![](./assets/week9/9.jpeg)

This app is super functional and easy to use; I will be definitely applying it to my presentations and projects.
This week, as most of them do, help us dive in further into new areas but also shape our ideas, interests and projects further. This week has actually helped me focus my project more and especially the consultations with Mariana and Tomas have been very beneficial. I am being exposed to amazing resources and very inspiring people; it’s up to me to put all of this information together and make it work into my final project. Excited and terrified of the final outcome. Fingers crossed.
