#Week6

**What is Intellegence?**

Vesa Gashi - Week 6

This was the first question and group assignment to this week’s class.
We discussed this in groups and each one came up with a bundle of words/definitions as to how they perceive intelligence to be.

Our group came up with a variety of definitions:
-Intelligence is the ability to make rational and logical decisions upon issues
-The ability to learn from previous mistakes in a combinational with emotional and logical thinking.
-Different senses able to absorb and understand different types of information.
-Ability to process information based in previous experiences and logical presumptions.
-Intelligence is an ever evolving state of perceiving the developments in our surrounding.
-Apart from the independent logical decisions and acts, the ability to respond towards external influences and factors accordingly.

If the exercise would’ve continued longer we would’ve come up with an entire chapter of definitions.

The discussion became more interesting as we proceeded into the class discussion and questions came up, specifically related to the last issue we were discussing in our group.
Does anything that has the ability to respond to an external factor considered intelligent? Can Slime be considered as intelligent? Are memory foam mattresses intelligent?

From an intro to Turings and Searles experiments to philosophical takes from Artistotle vs Plato on knowledge and rationalism, this week seemed to send us of into another journey with front row tickets into the future and the unknown.

From the first class I started thinking if there were ways to incorporate AI into my project. Would there be a need for it? My project since the beginning was deeply grounded in physical contact and human interaction after all. I was thinking that maybe through some sort of device/game, basically help of technology, I could foster the relationships I want among communities.

Before I could process and live with the information of the first two days, the course kept getting more intense and deeper into the Artificial Intelligence world and the morality of it all. Instead of processing and reaffirming information, new questions and moral dilemmas came up every day.  

It was interesting to see this play out in the group discussions and projects that we came up with during the last three days of the course. My group focused on developing a device/app that helps combat loneliness. A bit contradictory that we would need the help of AI to actually combat human loneliness but nonetheless we went ahead with the concept.

At first I wasn’t really aware of the way this was affecting me but now taking the time to process all the information, this silly exercise and project actually is somehow related to the shape my project has taken today. Still I’m not sure if it affected my subconscious thinking or if it’s just a coincidence that things are unfolding this way. (This process will be documented in the week Material Speculation.)
For now I’m noting that my project overall deals with fostering better human interaction. Building communities and giving them a sense of belongings to that space and each other. Bringing back that sacred feeling of belonging and taking care of the common good.

Notes from Day 3 look like this exactly:
(Considering forms on how to combine AI and Public Spaces within neighborhoods started exactly here, hint: **Public Spaces *Illustrations and *Interactions* Is Public Space relative to my project? *Should they be in rotation?*which city should I implement it on *Functioning of spaces?** etc.)

![](./assets/week6/Day3notes.jpeg)

![](./assets/week6/Day3notes1.jpeg)
On this day we dived deeper into Machine Learning: The Supervised, The unsupervised, The Reinforced. It was a good insight on how it functions and how it has branched out throughout the years.

**(Ironically) The Google definition of Machine Learning is:**
“Machine learning is a method of data analysis that automates analytical model building. It is a branch of artificial intelligence based on the idea that systems can learn from data, identify patterns and make decisions with minimal human intervention”

**Further than that:**
Machine learning uses algorithms to parse data, learn from that data, and make informed decisions based on what it has learned. Deep learning structures algorithms in layers to create an “artificial neural network” that can learn and make intelligent decisions on its own. Deep learning is a subfield of machine learning.

The Deep Learning part made the entire surreal world of AI and privacy issues seem far too real and gave us all a big nudge into understanding the vocal engineers and journalists who constantly talk about privacy issues and possible futures where machines spiral out of control. It made it seemed like that future is here and now and even worse.

From the Amazon algorithms, Instant citizen recognition/ behavior recording software’s used in China, to your friends’ suggestions on Social Media, all being crafted and controlled by algorithms generated from AI. We knew these things existed but I personally rarely thought much about how they were shaping our daily lives and how they were disrupting our behaviors, if nothing else, at times I found them useful in my busy schedule. “Thanks for the enlightenment Amazon, sure I’ll buy that shampoo with my vitamins, I guess I need it but didn’t know”.

There are times when AI is useful and makes our lives easier and there are points where its invasion goes way too far. The good news is that we still (somewhat) can control how much we want our daily decisions affected by big corporations and AI. As a society we can build frameworks to limit their power and invasion but we have to act fast, AI is being developed at stratospheric speed whereas laws and regulations that control their limits are abnormally slow. It remains upon ordinary citizens to oversee and pressure governmental bodies to pass stronger and clearer regulations to ensure our privacies and safety. There are plenty of examples that demonstrate how AI can be used to do both good and bad, it really just depends on our will and effort to determine which path will develop further.

**Overall this entire document was meant to be written from the Intersecting perspective between gender equality and AI but will leave it for now, as this document is very long due and I am dwelling into issues that are far off topic for the moment. These are the basic notes I had from this week, although these notes don’t do the class any justice to the impact it has had and the debates it has stirred. Will make revisions to this draft and add my thoughts further to it.**

Thanks to the lecturers for being so resourceful and having crafted a comprehendible lecture on a complex issue in only 5 days.  
